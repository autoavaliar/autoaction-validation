<?php
declare(strict_types=1);

namespace AutoAction\Validation\Exception;
use Throwable;

/**
 * Class ValidationFailsException
 *
 * @package AutoAction\Validation
 * @date    08/10/2018
 *
 * @author  Lucas Trindade <lucas.silva@autoavaliar.com.br>
 */
class ValidationFailsException extends \Exception
{
    /**
     * Array de erros de validação
     *
     * @var array $errors
     */
    private $errors;

    /**
     * ValidationFailsException constructor.
     *
     * @param array $errors
     * @param string $message
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct(array $errors, string $message = "", int $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);

        $this->errors = $errors;
    }

    /**
     * Devolve o array de erros
     *
     * @return array
     */
    public function errors(): array
    {
        return $this->errors;
    }
}