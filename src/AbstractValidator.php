<?php
declare(strict_types=1);

namespace AutoAction\Validation;

use AutoAction\Validation\Exception\ValidationFailsException;
use Rakit\Validation\Validator;

/**
 * Class AbstractValidator
 *
 * @package AutoAction\Validation
 * @date    08/10/2018
 *
 * @author  Lucas Trindade <lucas.silva@autoavaliar.com.br>
 */
abstract class AbstractValidator
{
    /**
     * Define as regras para validação
     *
     * @return array
     */
    abstract protected function rules(): array;

    /**
     * Efetua a validação dos dados
     *
     * @param array $data
     * @throws ValidationFailsException
     */
    public function validate(array $data)
    {
        $validator = new Validator();
        $validation = $validator->validate($data, $this->rules());
        if($validation->fails()){
            $errors = $this->errorsArray($validation->errors()->toArray());
            throw new ValidationFailsException($errors);
        }
    }

    /**
     * Monta estrutura do array de erros
     *
     * @param array $errors
     * @return array
     */
    private function errorsArray(array $errors): array
    {
        $array = [];
        foreach($errors as $field => $message){
            $array[] = [
                'field' => $field,
                'message' => array_pop($message)
            ];
        }

        return $array;
    }
}